/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.sample.resourceenum;

import java.util.Locale;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;
import org.vermeerlab.sample.resourceenum.config.Systema;
import org.vermeerlab.sample.resourceenum.config.Systemb;

/**
 *
 * @author Yamashita,Takahiro
 */
public class MessageTest {

    @Test
    public void messageToString() {
        Assert.assertThat(Systema.MSG001.toString(), is("システムAメッセージ００１"));
    }

    @Test
    public void messageFormatEnglish() {
        //対象ロケールのリソースが無いのでデフォルトロケールを参照
        Assert.assertThat(Systemb.MSG001.format(Locale.ENGLISH), is("システムBメッセージ００１"));
    }

}
